+++
author = "Mostafa"
title = "OSSTMM"
date = "2020-09-09"
description = "OSSTMM, please"
categories = [
    "Security",
]
tags = [
    "OSSTMM",
    "OpSec",
]
+++

*T*he *O*pen *S*ource *S*ecurity *T*esting *M*ethodology *M*anual

There is no complex subject for which the simplification process is not itself complex nor the
end result significantly less than the whole. This means that to make a security testing solution simple
enough for non-experts to execute, the solution requires a complex back-end to collect the data
according to preconceived rules.

**In art, the end result is a thing of beauty,
whereas in science, the means of
reaching the end result is a thing of
beauty. When a security test is an art then
the result is unverifiable and that
undermines the value of a test. One way
to assure a security test has value is to
know the test has been properly
conducted. For that you need to use a
formal methodology. The OSSTMM aims to
be it.**

Since environments are significantly more complex than in years past due such things as remote operations,
virtualization, cloud computing, and other new infrastructure types, we can no longer think in simplistic tests
meant only for desktops, servers, or routing equipment. Therefore, the OSSTMM encompasses
tests from all channels - Human, Physical, Wireless, Telecommunications, and Data Networks.

** Projects **

1. [Source Code Analysis Risk Evaluation (SCARE)](http://www.isecom.org/scare)
2. [Home Security Methodology and Vacation Guide (HSM)](http://www.isecom.org/hsm)
3. [Hacker Highschool (HHS)](http://www.hackerhighschool.org/) This is lovely.
4. [The Bad People Project (BPP)](http://www.badpeopleproject.org/) This one is strange!
5. [Security Operations Maturity Architecture (SOMA)](http://www.isecom.org/soma)
6. [Business Integrity Testing (BIT)](http://www.isecom.org/bit)
7. [Smarter Safer Better](http://www.smartersaferbetter.org/)
8. [Mastering Trust](http://www.isecom.org/seminars)

      Term           | Definition
   ------------------|-----------------------------------------------------------
   Attack Surface    | The lack of specific separations and functional controls that exist for that vector.
   Attack Vector     | A sub-scope of a vector created in order to approach the security testing of a complex scope in an organized manner.
   Controls          | The assurance that the physical and information assets as well as the channels themselves are protected.
   Limitations       | The current state of perceived and known limits for channels, operations, and controls
   Operations        | The lack of security one must have to be interactive, useful, public, open, or available.
   Perfect Security  | The exact balance of security and controls with operations and limitations.
   Porosity          | All interactive points, operations, which are categorized as a Visibility, Access, or Trust.
   Safety            | A form of protection where the threat or its effects are controlled.
   Security          | A form of protection where a separation is created between the assets and the threat.
   Rav               | A scale measurement of an attack surface.
   Target            | That within the scope that you are attacking, which is comprised of the asset and any protections the asset may have.
   Vector            | The direction of an interaction.
   Vulnerability     | One classification of Limitation where a person or process can access, deny access to others, or hide itself or assets within the scope.
   Identificarion    | The verification of an existing identity.
   Authorization     | The granting of permissions form the rightful authority.


## Just because you can’t directly control it doesn’t 
## mean it can’t be controlled. Control the environment 
## and you control everything in it.
