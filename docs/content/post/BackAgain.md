+++
author = "Mostafa Hesari"
title = "Back Again!"
date = "2022-05-24"
description = "I've always wanted to be more active online but ..."
tags = [
    "Hugo",
    "Gitlab",
]
categories = [
    "themes",
    "syntax",
]
series = ["Themes Guide"]
aliases = ["migrate-from-jekyl"]
+++

Today, started as a normal day which could end as difficult as possible.
<!--more-->
salam
## Email
Every email we send needs to be checked once, AT THE END!
One time is enough and vital.
Some typo may not result in losing your job but impacts you impression on people.
Last but not least, misundestanting is not productive!

## Gitlab SSH Connection
It could be a bit tricky but not a difficult one.
Every thing you do needs be documented.

Generate a SSH key, 
  ```

  ```
Then search for it... not a big deal but make life much easier.

  ### The rest is not important


## Python 3.11 is going to be much faster!
I never used pyperformance library.

I did the following:
```
sudo apt install python3.10-venv
mkdir ~/.venv 
python3 -m venv ~/.venv/playGraound
source ~/.venv/playGround/bin/activate
pip install pyperformance
pyperformance run -o py310.json
```  
It is running now! It takes for ever to benchmark python 3.10.
I will not run it for python 3.11 

Actually I dont care if python itself gets faster, the development time is important for me to be faster. 
Looking forward to see anything that make development easier.

Still running!

Have a good day.
