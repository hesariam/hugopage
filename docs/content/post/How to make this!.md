+++
author = "Mostafa Hesari"
title = "How to make this!"
date = "2020-09-11"
description = "Explaining how to make such awebsite with Hugo and Gitlab"
tags = [
    "Hugo",
    "Gitlab",
]
categories = [
    "themes",
    "syntax",
]
series = ["Themes Guide"]
aliases = ["migrate-from-jekyl"]
+++

This is an explanation about the process of making such a website!
<!--more-->

## Hugo
You need to understand how Hugo works. It might takes 5min to get the gist of it and play with it when building the website.
  Check the hugo website.
  Install it. Considering the versions regarding to theme conmatibility and so on.
    hugo new site { site name }
  Check out the themes online 
  Download the one you want to the sitename/theme directory
  Replace the config file in the theme directory with the one in the main directory
  Edit the new config.toml ( it could be .yaml or .json ) 
  Then run the local server to check it out. 
    hugo server 

In the directory that your main sitename directory located
  Create gitlab.ci.yml :
      ``` 
      stages:
        - publish

      variables:
        GIT_SUBMODULE_STRATEGY: recursive

      pages:
        stage: publish
        image: jojomi/hugo
        cache: {}
        script:
        - hugo -s docs            # Render the Hugo site
        - mv docs/public .        # Move the public folder into place
        artifacts:
          paths:
            - public              # Tell GitLab Pages to serve the public folder
        only:
        - master
      ```

## Gitlab
Set ssh for the git lab. ( if you do not konw about ssh, maybe this is the time to learn it. It takes 2 hours to get and couple of time practice to get handy not master!)

In the directory that sitename directory is located:
  ```
  git init .
  git add *
  git commit -m "First commit"
  git remote add { your peoject address}
  git push
  ```
Now you have the web site ! Yo.

  ### The rest is not important



## Blockquotes

The blockquote element represents content that is quoted from another source, optionally with a citation which must be within a `footer` or `cite` element, and optionally with in-line changes such as annotations and abbreviations.

#### Blockquote without attribution

> Tiam, ad mint andaepu dandae nostion secatur sequo quae.
> **Note** that you can use *Markdown syntax* within a blockquote.

#### Blockquote with attribution

> Don't communicate by sharing memory, share memory by communicating.</p>
> — <cite>Rob Pike[^1]</cite>


[^1]: The above quote is excerpted from Rob Pike's [talk](https://www.youtube.com/watch?v=PAAkCSZUG1c) during Gopherfest, November 18, 2015.

## Tables

Tables aren't part of the core Markdown spec, but Hugo supports supports them out-of-the-box.

   Name | Age
--------|------
    Bob | 27
  Alice | 23

#### Inline Markdown within tables

| Inline&nbsp;&nbsp;&nbsp;     | Markdown&nbsp;&nbsp;&nbsp;  | In&nbsp;&nbsp;&nbsp;                | Table      |
| ---------- | --------- | ----------------- | ---------- |
| *italics*  | **bold**  | ~~strikethrough~~&nbsp;&nbsp;&nbsp; | `code`     |

## Code Blocks

#### Code block with backticks

```
html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
```
#### Code block indented with four spaces

    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>Example HTML5 Document</title>
    </head>
    <body>
      <p>Test</p>
    </body>
    </html>

#### Code block with Hugo's internal highlight shortcode
{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
{{< /highlight >}}

## List Types

#### Ordered List

1. First item
2. Second item
3. Third item

#### Unordered List

* List item
* Another item
* And another item

#### Nested list

* Item
1. First Sub-item
2. Second Sub-item

## Other Elements — abbr, sub, sup, kbd, mark

<abbr title="Graphics Interchange Format">GIF</abbr> is a bitmap image format.

H<sub>2</sub>O

X<sup>n</sup> + Y<sup>n</sup> = Z<sup>n</sup>

Press <kbd><kbd>CTRL</kbd>+<kbd>ALT</kbd>+<kbd>Delete</kbd></kbd> to end the session.

Most <mark>salamanders</mark> are nocturnal, and hunt for insects, worms, and other small creatures.

